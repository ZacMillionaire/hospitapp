<?php
	include "inc/header.php";
?>
<?php
	include "inc/menu.php";
	// Page content start
?>
		<div id="content-container">
			<div id="sub-page-menu">
				<ul>
					<li><a href="<?php echo ($_GET["m"]=="staff") ? "staff.php" : "patients.php"; ?>">&laquo; Go back</a></li>
				</ul>
			</div>
			<div id="entry-form">
				<?php

					// Split staff and patients into their own pages, too messy otherwise

					if($_GET['m']=="staff") {

						$formDetails = $Users->fetchUserFullData($_GET['id']);
						$formDetails = $formDetails[0];

				?>
				<h1>Editing Staff Member: {title} <?php echo $formDetails['first_name']." ".$formDetails['last_name']; ?> </h1>
				<form action="handles/update-staff.php?id=<?php echo $_GET['id']; ?>" method="POST">
				<?php
					} else {

						$formDetails = $Users->fetchPatientFullData($_GET['id']);
						$formDetails = $formDetails[0];
				?>
				<h1>Editing Patient {name}</h1>
				<form action="handles/update-patient.php?id=<?php echo $_GET['id']; ?>" method="POST">
				<?php
					}
				?>
					<div id="left-side">
						<div class="form-row generic-row">
							<div class="form-label generic">
								<label for="first-name">First Name</label>
							</div>
							<div class="form-input generic">
								<input id="first-name" type="text" value="<?php echo $formDetails['first_name']; ?>" placeholder="First Name" name="first-name"/>
							</div>
						</div>

						<div class="form-row">
							<div class="form-label generic">
								<label for="last-name">Last Name</label>
							</div>
							<div class="form-input generic">
								<input id="last-name" type="text" value="<?php echo $formDetails['last_name']; ?>" placeholder="Last Name" name="last-name"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="street-number">Street Number</label>
							</div>
							<div class="form-input generic">
								<input id="street-number" type="text" value="<?php echo $formDetails['streetNumber']; ?>" placeholder="Last Name" name="street-number"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="street-name">Street Name</label>
							</div>
							<div class="form-input generic">
								<input id="street-name" type="text" value="<?php echo $formDetails['streetName']; ?>" placeholder="Street Name" name="street-name"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="suburb">Suburb</label>
							</div>
							<div class="form-input generic">
								<input id="suburb" type="text" value="<?php echo $formDetails['suburb']; ?>" placeholder="Suburb" name="suburb"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="state">State</label>
							</div>
							<div class="form-input generic">
								<input id="state" type="text" value="<?php echo $formDetails['state']; ?>" placeholder="State" name="state"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="country">Country</label>
							</div>
							<div class="form-input generic">
								<input id="country" type="text" value="<?php echo $formDetails['country']; ?>" placeholder="country" name="country"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="post-code">Post Code</label>
							</div>
							<div class="form-input generic">
								<input id="post-code" type="text" value="<?php echo $formDetails['postCode']; ?>" placeholder="Post Code" name="post-code"/>
							</div>
						</div>
						
					</div>
					<div id="right-side">
						<div class="form-row generic-row">
							<div class="form-label generic">
								<label for="home-phone">Home Phone</label>
							</div>
							<div class="form-input generic">
								<input id="home-phone" type="text" value="<?php echo $formDetails['homePhone']; ?>" placeholder="Home Phone" name="home-phone"/>
							</div>
						</div>

						<div class="form-row">
							<div class="form-label generic">
								<label for="mobile-phone">Mobile Phone</label>
							</div>
							<div class="form-input generic">
								<input id="mobile-phone" type="text" value="<?php echo $formDetails['mobilePhone']; ?>" placeholder="Mobile Phone" name="mobile-phone"/>
							</div>
						</div>

						<?php
							if($_GET['m']=="staff") {
						?>
						<div class="form-row">
							<div class="form-label generic">
								<label for="qualification">Qualifications</label>
							</div>
							<div class="form-input generic">
								<input id="qualification" type="text" value="<?php echo $formDetails['qualifications']; ?>" placeholder="Qualifications" name="qualification"/>
							</div>
						</div>
						<?php
							}
						?>
						<div class="form-row">
							<div class="form-label generic">
								<label for="sex">Sex</label>
							</div>
							<div class="form-input generic">
								<select name="sex" id="sex">
									<option value="male" <?php echo ($formDetails['sex'] == "male") ? 'selected' : false; ?> >Male</option>
									<option value="female" <?php echo ($formDetails['sex'] == "female") ? 'selected' : false; ?> >Female</option>
								</select>
							</div>
						</div>

						<div class="form-row">
							<div class="form-label generic">
								<label for="date-of-birth">Date Of Birth</label>
							</div>
							<div class="form-input generic">
								<input id="date-of-birth" value="<?php echo $formDetails['DOB']; ?>" type="date" name="date-of-birth"/>
							</div>
						</div>
						<?php
							if($_GET['m']=="staff") {
						?>
						<div class="form-row">
							<div class="form-label generic">
								<label for="position">Position</label>
							</div>
							<div class="form-input generic">
								<select name="position" id="position">
									<option value="doctor">Doctor</option>
									<option value="nurse">Nurse</option>
									<option value="receptionist">Receptionist</option>
									<option value="whatever">Whatever</option>
								</select>
							</div>
						</div>
						<?php
							}
						?>

					</div>
						<?php
						// tidy this shit up
							if($_GET['m']=="staff") {
						?>
					<div class="form-row">
						<h1>Change password?</h1>
						<h3>Only enter a password if you wish to change a password.</h3>
					</div>
					<div class="form-row">
						<div class="form-label generic">
							<label for="qualification">Password</label>
						</div>
						<div class="form-input generic">
							<input id="qualification" type="password" placeholder="New Password" name="new-password-first"/>
						</div>
					</div>
					<div class="form-row">
						<div class="form-label generic">
							<label for="qualification">Password (again)</label>
						</div>
						<div class="form-input generic">
							<input id="qualification" type="password" placeholder="New Password (again)" name="new-password-again"/>
						</div>
					</div>
						<?php
							}
						?>
					<div class="form-row">
						<div class="form-button">
							<button>Update This Patient</button>
						</div>
					</div>
				</form>
			</div>
		</div>
<?php
	// Page content end
	include "inc/footer.php";
?>
