<?php
	include "inc/header.php";
?>
<?php
	include "inc/menu.php";
	// Page content start
?>
		<div id="content-container">
			<div class="module-box">
				<div class="module-box-header">Rooms</div>
				<div class="module-box-content">Beds free: ##</div>
			</div>
			<div class="module-box">
				<div class="module-box-header">Staff</div>
				<div class="module-box-content">
					Doctors: ##<br>
					Nurses: ##
				</div>
			</div>
			<div class="module-box">
				<div class="module-box-header">Box Header</div>
				<div class="module-box-content">Box Content</div>
			</div>
			<div class="module-box">
				<div class="module-box-header">Box Header</div>
				<div class="module-box-content">Box Content</div>
			</div>
		</div>
<?php
	// Page content end
	include "inc/footer.php";
?>
