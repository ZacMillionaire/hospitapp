<?php
	include "inc/header.php";
?>
		<div id="login-container">
			<?php
				if(isset($_GET['e']) && $_GET['e'] == 'true') {
			?>
			<div id="error">
				Incorrect username/password combination.
			</div>
			<?php
				}
			?>
			<form action="handles/check_login.php" method="POST">
				<div class="form-row">
					<div class="form-input">
						<input class="login" id="username" type="text" placeholder="Username" name="username"/>
					</div>
				</div>
				<div class="form-row">
					<div class="form-input">
						<input class="login" id="password" type="password" placeholder="Password" name="password"/>
					</div>
				</div>
				<div class="form-row">
					<div class="form-button">
						<button>Log in</button>
					</div>
				</div>
			</form>
		</div>
<?php
	// Page content end
	include "inc/footer.php";
?>