<?php
	require_once "sys/system.php";

	class DataCollection {

		private $db;
		public $dataCollection;

		public function __construct() {

			global $Database;

			$argList = func_get_args();
			$argList = explode(',', $argList[0]);

			$this->db = $Database;

			foreach ($argList as $key => $value) {
				call_user_func("self::".$value);
			}
			$this->returnDataCollection();
		}

		private function staffDataCollection() {
			$sql = "SELECT *
					FROM `staff`
					ORDER BY `last_name` ASC
					";
			$params = null;
			$result = $this->db->dbQuery($sql,$params);

			foreach ($result as $key => $value) {
				$groupedArray[$value['last_name'][0]][] = $value;
			}

			$this->dataCollection['staff'] = $groupedArray;

		}

		private function patientDataCollection() {
			$sql = "SELECT *
					FROM `patients`
					ORDER BY `last_name` ASC
					";
			$params = null;
			$result = $this->db->dbQuery($sql,$params);

			if($result) {
				foreach ($result as $key => $value) {
					$groupedArray[$value['last_name'][0]][] = $value;
				}

				$this->dataCollection['patients'] = $groupedArray;			
			} else {
				$this->dataCollection['patients'] = false;
			}

		}

		private function returnDataCollection() {
			return $this->dataCollection;
			// header('Content-Type: application/json');
			// echo json_encode($this->dataCollection);	
		}

	}
?>