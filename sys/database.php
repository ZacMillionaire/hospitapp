<?php
	
	class Database {
	
		protected $database;
		
		public function __construct($host,$database,$user,$password) {
			$this->database = new PDO("mysql:host=".$host.";dbname=".$database,$user,$password);
			$this->database->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
		}
		
		public function dbQuery($query,$params=null) {
			try {
				$queryExec = $this->database->prepare($query);
				$queryExec->execute($params);
				$row = $queryExec->fetchAll(PDO::FETCH_ASSOC);
				return $row;
			} catch(PDOException $ex){
				return "Query Failed. ".$ex->getMessage();
			}
		}	
		public function dbInsert($query,$params=null) {
			try {
				$queryExec = $this->database->prepare($query);
				$queryExec->execute($params);
			} catch(PDOException $ex){
				return false;
				return "Query Failed. ".$ex->getMessage();
			}
			return true;
		}	
	}
?>