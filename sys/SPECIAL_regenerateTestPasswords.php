<?php
	include "../sys/system.php";
	//$Users->checkUserAuth();
	$staff =$Database->dbQuery("SELECT * FROM `staff`");
	foreach ($staff as $key => $value) {
		$username = strtolower($value['first_name'][0].$value['last_name']).rand(1,10);
		$passwordHashes = $Users->generateNewPassword("password");
		$userID = $value['staffID'];
		echo "$userID > $username | $passwordHashes[0] | $passwordHashes[1] <br/>";
		$sqlQuery = "UPDATE `staff` SET `username` = :username, `passwordSalt` = :salt, `passwordHash` = :hash WHERE `staffID` = :ID";
		$params = array(
			"ID" => $userID,
			"username" => $username,
			"hash" => $passwordHashes[0],
			"salt" => $passwordHashes[1]
		);
		$Database->dbQuery($sqlQuery,$params);
	}
?>