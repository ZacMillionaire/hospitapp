<?php
	
	require_once "database.php";

	$Systems = new Systems();
	if($_SERVER['HTTP_HOST']=='localhost') {
		$Database = new database("localhost","qut_inb201","root","");
	} else {
		$Database = new database("localhost","jotunga_INB201","jotunga_INB201","5gLd2CfTb9CK");
	}
	$Users = new Users($Systems,$Database);

	class Systems {

		function __construct() {
		}
		function getCurrentPage() {
			$pageSelf = preg_match_all("^([0-9a-zA-Z-]*\.php)^", $_SERVER['PHP_SELF'],$matches);
			$pageSelf = $matches[0][0];
			return $pageSelf;
		}
		function getRootLocation() {
			$root = "http://".$_SERVER['SERVER_NAME']."/INB201/2014-03-25";
			return $root;
		}
	}
	class Users {
		function __construct($sys,$db) {
			$this->Systems = $sys;
			$this->Database = $db;
			$this->checkUserLogin();
		}

		function checkUserAuth($postData) {
			$this->findUserName($postData);
		}

		function checkUserLogin() {

			$loginRequired = array('index.php','staff.php');
			$loginReject = array('login.php');

			$pageSelf = $this->Systems->getCurrentPage();

			if(isset($_COOKIE['loginhash'])) {
				//echo "cookie set";
				$loginCheck = $this->getLoginHash($_COOKIE['loginhash']);
				if(!$loginCheck) {
					$this->logUserOut($_COOKIE['loginhash']);
				}
				if(in_array($pageSelf, $loginRequired) && $_COOKIE['loginhash']){
					//echo "Accept Access";
				} elseif (in_array($pageSelf, $loginReject)) {
					//echo "Reject Access";
					header("Location: ".$this->Systems->getRootLocation());
				}
			} elseif(!isset($_COOKIE['loginhash']) && $pageSelf != 'login.php') {
				header("Location: ".$this->Systems->getRootLocation()."/login.php");	
			}

		}

		function logUserOut($hash) {
			$query = "DELETE FROM `staff_loggedin` WHERE `staffID` = (SELECT `staffID` FROM `staff` WHERE `username` = :username)";
			$params = array(
				"username" => $_COOKIE['staffID']
			);
			$this->Database->dbQuery($query,$params);
			setcookie("loginhash","",strtotime( '-31 days' ),"/INB201");
			setcookie("staffID","",strtotime( '-31 days' ),"/INB201");
			header("Location: ".$this->Systems->getRootLocation()."/login.php?m=logout");
		}

		function getLoginHash($loginHash) {
			//print_r($loginHash);
			$query = "SELECT * FROM `staff_loggedin` WHERE `loginHash` = :hash";
			$params = array(
				"hash" => $loginHash
			);
			$result = $this->Database->dbQuery($query,$params);
			if($result[0]['loginHash'] === $loginHash) {
				return true;
			} else {
				return false;
			}
		}

		function generateNewPassword($password) {
			$password = hash("sha512", $password);
			$passwordSalt = hash("sha512",mt_rand());
			$passwordHash = hash("sha512",$password.$passwordSalt);

			return $hashes = array($passwordHash,$passwordSalt);
		}

		function findUserName($postData) {
			$query = "SELECT `passwordSalt`,`passwordHash` FROM `staff` WHERE `username` = :username";
			$params = array(
				"username" => trim($postData['username'])
			);
			$result = $this->Database->dbQuery($query,$params);
			if($result) {
				$this->validatePassword($result[0],$postData['password'],$postData['username']);
			} else {
				header("Location: ".$this->Systems->getRootLocation()."/login.php?e=true");
			}
		}

		function validatePassword($passwordArray,$plainTextPassword,$username) {
			$plainTextPasswordHash = hash("sha512",$plainTextPassword);
			$passwordHash = hash("sha512",$plainTextPasswordHash.$passwordArray["passwordSalt"]);

			if($passwordHash == $passwordArray["passwordHash"]) {
				//echo "valid password. Gen cookie";
				$this->generateCookie($username);
			} else {
				header("Location: ".$this->Systems->getRootLocation()."/login.php?e=true");
			}
		}

		function generateCookie($username) {
			$loginHash = hash("sha512",time().$username);
			setcookie("loginhash",$loginHash,strtotime( '+30 days' ),"/INB201");
			setcookie("staffID",$username,strtotime( '+30 days' ),"/INB201");
			$query = "INSERT INTO `staff_loggedin` (`staffID`,`loginHash`) VALUES ((SELECT `staffID` FROM `staff` WHERE `username` = :username),:hash);";
			$params = array(
				"username" => $username,
				"hash" => $loginHash
			);
			if($this->Database->dbInsert($query,$params)) {
				header("Location: ".$this->Systems->getRootLocation());
			}
		}

		function createNewUser($formData) {
			$passwordHashes = $this->generateNewPassword("password");
			$query = "INSERT INTO `staff` (`first_name`,`last_name`,`username`,`passwordSalt`,`passwordHash`) VALUES (:fname, :lname, :uname, :salt, :hash);";

			$username = strtolower($formData['first-name'][0].$formData['last-name']).rand(1,10);

			$params = array(
				"fname" => ucfirst($formData['first-name']),
				"lname" => ucfirst($formData['last-name']),
				"uname" => $username,
				"hash" => $passwordHashes[0],
				"salt" => $passwordHashes[1]
			);
			if($this->Database->dbInsert($query,$params)) {
				//fetchInsertedUser($username);
				$this->insertStaffPersonalInformation($formData,$username);
				$userID = $this->fetchUser($username,true);

				// change this to go to a 'successfully created user' or whatever page, with a link to the below
				header("Location: ".$this->Systems->getRootLocation()."/view-staff.php?id=".$userID[0]['staffID']);
				return true;
			} else {
				return false;
			}
		}

		function updateUser($formData) {
			//print_r($formData);

			if(trim($formData['new-password-first']) && trim($formData['new-password-again'])) {
				//echo "Both passwords set<br/>";
				if($formData['new-password-first'] === $formData['new-password-again']) {
					//echo "both passwords match<br/>";
					$this->updateUserTable($formData,true);
				} else {
					//echo "You had one job idiot<br/>";
				}
			} elseif(!trim($formData['new-password-first']) && !trim($formData['new-password-again'])) {
				//echo "neither password set<br/>";
				$this->updateUserTable($formData,false);
			} else {
				//echo "one of them is set<br/>";
			}
			$this->updateUserPersonalInformation($formData);
			header("Location: ".$this->Systems->getRootLocation()."/view-staff.php?id=$_GET[id]");

			die();

			if($this->Database->dbInsert($query,$params)) {
				//fetchInsertedUser($username);
				$this->insertStaffPersonalInformation($formData,$username);
				header("Location: ".$this->Systems->getRootLocation()."/view-staff.php?uname=$username");
				return true;
			} else {
				return false;
			}
		}

		function updateUserPersonalInformation($formData) {
			//echo "fuck ya right";
			$query = "UPDATE `personalinformation` SET
						  `streetNumber` = :snumber,
						  `streetName` = :sname,
						  `suburb` = :suburb,
						  `state` = :state,
						  `country` = :country,
						  `postCode` = :postcode,
						  `homePhone` = :homephone,
						  `mobilePhone` = :mobilephone,
						  `qualifications` = :qualifications,
						  `sex` = :sex,
						  `DOB` = :dob
					  WHERE `personalInformationID` = (
						  SELECT `personalInformationID` FROM `staff_personalinformation` 
						  INNER JOIN `staff` ON `staff`.`staffID` = `staff_personalinformation`.`staffID`
						  WHERE `staff_personalinformation`.`staffID` = :id
					  );";
			$params = array(
				"snumber" => $formData['street-number'],
				"sname" => $formData['street-name'],
				"suburb" => $formData['suburb'],
				"state" => $formData['state'],
				"country" => $formData['country'],
				"postcode" => $formData['post-code'],
				"homephone" => $formData['home-phone'],
				"mobilephone" => $formData['mobile-phone'],
				"qualifications" => $formData['qualification'],
				"sex" => $formData['sex'],
				"dob" => $formData['date-of-birth'],
				"id" => $_GET['id']
			);
			$this->Database->dbInsert($query,$params);
		}

		function updateUserTable($formData,$passwordsChanged = false) {

			// $query = "UPDATE `staff` SET
			// 			  `first_name` = :fname,
			// 			  `last_name` = :lname,
			// 			  `username` = :uname,
			// 			  `passwordSalt` = :salt,
			// 			  `passwordHash` = :hash
			// 		  WHERE `staffID` = :id;";
			//echo "What up dickhead<br/>";
			if($passwordsChanged) {
				$passwordHashes = $this->generateNewPassword($formData['new-password-first']);

				$query = "UPDATE `staff` SET
							  `first_name` = :fname,
							  `last_name` = :lname,
							  `passwordSalt` = :salt,
							  `passwordHash` = :hash
						  WHERE `staffID` = :id;";

				$params = array(
					"fname" => ucfirst($formData['first-name']),
					"lname" => ucfirst($formData['last-name']),
					//"uname" => $username, // not touching username yet
					"hash" => $passwordHashes[0],
					"salt" => $passwordHashes[1],
					"id" => $_GET['id']
				);
			} else {
				$passwordHashes = $this->generateNewPassword($formData['new-password-first']);

				$query = "UPDATE `staff` SET
							  `first_name` = :fname,
							  `last_name` = :lname
						  WHERE `staffID` = :id;";
				$params = array(
					"fname" => ucfirst($formData['first-name']),
					"lname" => ucfirst($formData['last-name']),
					"id" => $_GET['id']
				);
			}

			$this->Database->dbInsert($query,$params);
		}

		function updatePatient($formData) {
			//print_r($formData);

			$this->updatePatientTable($formData);
			$this->updatePatientPersonalInformation($formData);

			header("Location: ".$this->Systems->getRootLocation()."/view-patient.php?id=$_GET[id]");

			die();
		}

		function updatePatientTable($formData) {

			// $query = "UPDATE `staff` SET
			// 			  `first_name` = :fname,
			// 			  `last_name` = :lname,
			// 			  `username` = :uname,
			// 			  `passwordSalt` = :salt,
			// 			  `passwordHash` = :hash
			// 		  WHERE `staffID` = :id;";
			//echo "What up dickhead<br/>";

			$query = "UPDATE `patients` SET
						  `first_name` = :fname,
						  `last_name` = :lname
					  WHERE `patientID` = :id;";

			$params = array(
				"fname" => ucfirst($formData['first-name']),
				"lname" => ucfirst($formData['last-name']),
				"id" => $_GET['id']
			);
			//die();

			$this->Database->dbInsert($query,$params);
		}

		function updatePatientPersonalInformation($formData) {
			//echo "fuck ya right";
			$query = "UPDATE `personalinformation` SET
						  `streetNumber` = :snumber,
						  `streetName` = :sname,
						  `suburb` = :suburb,
						  `state` = :state,
						  `country` = :country,
						  `postCode` = :postcode,
						  `homePhone` = :homephone,
						  `mobilePhone` = :mobilephone,
						  `sex` = :sex,
						  `DOB` = :dob
					  WHERE `personalInformationID` = (
						  SELECT `personalInformationID` FROM `patient_personalinformation` 
						  INNER JOIN `patients` ON `patients`.`patientID` = `patient_personalinformation`.`patientID`
						  WHERE `patient_personalinformation`.`patientID` = :id
					  );";
			$params = array(
				"snumber" => $formData['street-number'],
				"sname" => $formData['street-name'],
				"suburb" => $formData['suburb'],
				"state" => $formData['state'],
				"country" => $formData['country'],
				"postcode" => $formData['post-code'],
				"homephone" => $formData['home-phone'],
				"mobilephone" => $formData['mobile-phone'],
				"sex" => $formData['sex'],
				"dob" => $formData['date-of-birth'],
				"id" => $_GET['id']
			);
			$this->Database->dbInsert($query,$params);
		}

		function createNewPatient($formData) {

			$query = "INSERT INTO `patients` (`first_name`,`last_name`,`UID`) VALUES (:fname, :lname, :uid);";

			$UID = hash("sha512",$formData['first-name'].time().$formData['last-name']);

			$params = array(
				"fname" => ucfirst($formData['first-name']),
				"lname" => ucfirst($formData['last-name']),
				"uid" => $UID
			);
			if($this->Database->dbInsert($query,$params)) {
				//fetchInsertedUser($username);
				$this->insertPatientPersonalInformation($formData,$UID);

				$patientID = $this->fetchPatient($UID,true);

				// change this to go to a 'successfully created patient' or whatever page, with a link to the below
				header("Location: ".$this->Systems->getRootLocation()."/view-patient.php?id=".$patientID[0]['patientID']);

				//header("Location: ".$this->Systems->getRootLocation()."/view-patient.php?uid=$UID");
				return true;
			} else {
				return false;
			}
		}

		function fetchUser($identifier,$byUserName = false) {
			if($byUserName) {
				$query = "SELECT `staffID`,`first_name`,`last_name`,`username` FROM `staff` WHERE `username` = :uname";
				$params = array(
					"uname" => $identifier
				);
			} else {
				$query = "SELECT `staffID`,`first_name`,`last_name`,`username` FROM `staff` WHERE `staffID` = :id";
				$params = array(
					"id" => $identifier
				);
			}
			return $this->Database->dbQuery($query,$params);
		}
		function fetchUserFullData($id) {
			$query = "SELECT *
					  FROM `staff`
					  LEFT JOIN `staff_personalinformation` AS `spi` ON `spi`.`staffID` = `staff`.`staffID`
					  LEFT JOIN `personalinformation` ON `spi`.`personalinformationID` = `personalinformation`.`personalinformationID`
					  WHERE `staff`.`staffID` = :id";
			$params = array(
				"id" => $id
			);
			return $this->Database->dbQuery($query,$params);
		}

		function fetchPatient($identifier,$byPatientUID = false) {
			if($byPatientUID) {
				$query = "SELECT `patientID`,`first_name`,`last_name`,`UID` FROM `patients` WHERE `UID` = :uid";
				$params = array(
					"uid" => $identifier
				);
			} else {
				$query = "SELECT `patientID`,`first_name`,`last_name`,`UID` FROM `patients` WHERE `patientID` = :id";
				$params = array(
					"id" => $identifier
				);
			}
			return $this->Database->dbQuery($query,$params);
		}
		function fetchPatientFullData($id) {
			$query = "SELECT *
					  FROM `patients`
					  LEFT JOIN `patient_personalinformation` AS `ppi` ON `ppi`.`patientID` = `patients`.`patientID`
					  LEFT JOIN `personalinformation` ON `ppi`.`personalinformationID` = `personalinformation`.`personalinformationID`
					  WHERE `patients`.`patientID` = :id";
			$params = array(
				"id" => $id
			);
			return $this->Database->dbQuery($query,$params);
		}

		function insertStaffPersonalInformation($formData,$username) {
			$query = "INSERT INTO `personalinformation`(
						`streetNumber`,
						`streetName`,
						`suburb`,
						`state`,
						`country`,
						`postCode`,
						`homePhone`,
						`mobilePhone`,
						`qualifications`,
						`sex`,
						`DOB`
					) VALUES (
						:snumber,
						:sname,
						:suburb,
						:state,
						:country,
						:postcode,
						:homephone,
						:mobilephone,
						:qualifications,
						:sex,
						:dob
					);";

			$params = array(
				"snumber" => $formData['street-number'],
				"sname" => $formData['street-name'],
				"suburb" => $formData['suburb'],
				"state" => $formData['state'],
				"country" => $formData['country'],
				"postcode" => $formData['post-code'],
				"homephone" => $formData['home-phone'],
				"mobilephone" => $formData['mobile-phone'],
				"qualifications" => $formData['qualification'],
				"sex" => $formData['sex'],
				"dob" => $formData['date-of-birth']
			);

			if($this->Database->dbInsert($query,$params)) {

				$query = "SELECT `personalInformationID` FROM `personalinformation` ORDER BY `personalInformationID` DESC LIMIT 1;";

				$insertID = $this->Database->dbQuery($query,null);
				$insertID = $insertID[0]['personalInformationID'];

				$staffID = $this->fetchUser($username,true);
				$staffID = $staffID[0]['staffID'];

				$query = "INSERT INTO `staff_personalinformation` (`staffID`,`personalInformationID`) VALUES (:sid,:piid);";
				$params = array(
					"sid" => $staffID,
					"piid" => $insertID // Because this won't cause problems later on
				);

				if($this->Database->dbInsert($query,$params)) {
					//echo "fuck yeah";
				} else {
					//echo "fuck you";
				}

			} else {
				return false;
			}
		}
		function insertPatientPersonalInformation($formData,$uid) {
			$query = "INSERT INTO `personalinformation`(
						`streetNumber`,
						`streetName`,
						`suburb`,
						`state`,
						`country`,
						`postCode`,
						`homePhone`,
						`mobilePhone`,
						`qualifications`,
						`sex`,
						`DOB`
					) VALUES (
						:snumber,
						:sname,
						:suburb,
						:state,
						:country,
						:postcode,
						:homephone,
						:mobilephone,
						:qualifications,
						:sex,
						:dob
					);";

			$params = array(
				"snumber" => $formData['street-number'],
				"sname" => $formData['street-name'],
				"suburb" => $formData['suburb'],
				"state" => $formData['state'],
				"country" => $formData['country'],
				"postcode" => $formData['post-code'],
				"homephone" => $formData['home-phone'],
				"mobilephone" => $formData['mobile-phone'],
				"qualifications" => null,
				"sex" => $formData['sex'],
				"dob" => $formData['date-of-birth']
			);

			if($this->Database->dbInsert($query,$params)) {

				$query = "SELECT `personalInformationID` FROM `personalinformation` ORDER BY `personalInformationID` DESC LIMIT 1;";

				$insertID = $this->Database->dbQuery($query,null);
				$insertID = $insertID[0]['personalInformationID'];

				$query = "SELECT `patientID` FROM `patients` WHERE `UID` = :uid";
				$params = array(
					"uid" => $uid
				);
				$patientID = $this->Database->dbQuery($query,$params);
				$patientID = $patientID[0]['patientID'];

				$query = "INSERT INTO `patient_personalinformation` (`patientID`,`personalInformationID`) VALUES (:pid,:piid);";
				$params = array(
					"pid" => $patientID, // fuck you thats why
					"piid" => $insertID // Because this won't cause problems later on
				);

				if($this->Database->dbInsert($query,$params)) {
					//echo "fuck yeah";
				} else {
					//echo "fuck you";
				}

			} else {
				return false;
			}
		}
	}
?>