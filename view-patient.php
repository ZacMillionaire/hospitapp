<?php
	include "inc/header.php";
?>
<?php
	include "inc/menu.php";
	// Page content start
?>
		<div id="content-container">
			<div id="sub-page-menu">
				<ul>
					<li><a href="patients.php">&laquo; Go back</a></li>
				</ul>
			</div>
			<?php
				$patientDetails = $Users->fetchPatientFullData($_GET['id']);
				$patientDetails = $patientDetails[0];
				foreach($patientDetails as $key => $value) {
					if($value == null) {
						$patientDetails[$key] = "---";
					}
				}
				//print_r($Users->fetchPatient($_GET['id']));
			?>
			<div id="entry-form">
				<h1>Details for <?php echo $patientDetails['first_name']." ".$patientDetails['last_name']; ?></h1>
				<div id="left-side">
					<div class="form-row generic-row">
						<div class="form-label generic">
							<label for="first-name">First Name</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['first_name']; ?>
						</div>
					</div>

					<div class="form-row">
						<div class="form-label generic">
							<label for="last-name">Last Name</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['last_name']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="street-number">Street Number</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['streetNumber']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="street-name">Street Name</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['streetName']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="suburb">Suburb</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['suburb']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="state">State</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['state']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="country">Country</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['country']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="post-code">Post Code</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['postCode']; ?>
						</div>
					</div>
					
				</div>
				<div id="right-side">
					<div class="form-row generic-row">
						<div class="form-label generic">
							<label for="home-phone">Home Phone</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['homePhone']; ?>
						</div>
					</div>

					<div class="form-row">
						<div class="form-label generic">
							<label for="mobile-phone">Mobile Phone</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['mobilePhone']; ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-label generic">
							<label for="sex">Sex</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['sex']; ?>
						</div>
					</div>

					<div class="form-row">
						<div class="form-label generic">
							<label for="date-of-birth">Date Of Birth</label>
						</div>
						<div class="form-input generic">
							<?php echo $patientDetails['DOB']; ?>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-button">
						<!-- change this to a styled link -->
						<button onclick="window.location.href='edit.php?m=patient&amp;id=<?php echo $patientDetails["patientID"]; ?>'">Edit this user</button>
					</div>
				</div>
			</div>
		</div>
<?php
	// Page content end
	include "inc/footer.php";
?>
