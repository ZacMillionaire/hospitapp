<?php
	include "inc/header.php";
?>
<?php
	include "inc/menu.php";
	// Page content start
?>
		<div id="content-container">
			<div id="sub-page-menu">
				<ul>
					<li><a href="add-new.php?m=patient">Add New Patient</a></li>
				</ul>
			</div>
			<?php
				include "data/patient-data.php";
				if($data) {
					foreach ($data as $key => $value) {
			?>
				<div class="staff-block-container">
					<div class="staff-block-letter-container"><?php echo $key; ?></div>
					<div class="staff-block-inner-container">
						<?php
							foreach ($value as $key => $value) {
						?>
						<div class="staff-entry-container">
							<div class="staff-name-container">
								<a href="view-patient.php?id=<?php echo $value["patientID"]; ?>">
									<span class="last-name"><?php echo $value['last_name']; ?>, </span><span class="first-name"><?php echo $value['first_name']; ?></span>
								</a>
							</div>
							<div class="staff-details-container">Some details about this Patient: are they dead or whatever</div>
							<div class="staff-edit"><a href="edit.php?m=patient&amp;id=<?php echo $value["patientID"]; ?>">Edit</a></div>
						</div>

						<?php
							}
						?>
					</div>
				</div>
			<?php			
					}
				} else {
			?>
				No patients
			<?php
				}
			?>
		</div>
<?php
	// Page content end
	include "inc/footer.php";
?>
