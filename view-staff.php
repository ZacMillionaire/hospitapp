<?php
	include "inc/header.php";
?>
<?php
	include "inc/menu.php";
	// Page content start
?>
		<div id="content-container">
			<div id="sub-page-menu">
				<ul>
					<li><a href="staff.php">&laquo; Go back</a></li>
				</ul>
			</div>
			<?php
				$staffDetails = $Users->fetchUserFullData($_GET['id']);
				$staffDetails = $staffDetails[0];
				foreach($staffDetails as $key => $value) {
					if($value == null) {
						$staffDetails[$key] = "---";
					}
				}
				//print_r($Users->fetchUserFullData($_GET['id']));
			?>
			<div id="entry-form">
				<?php
					// Ghetto role system framework
					if($_GET['id'] != 1) {
				?>
				<h1>Details for {title} <?php echo $staffDetails['first_name']." ".$staffDetails['last_name']; ?></h1>
				<div id="left-side">
					<div class="form-row generic-row">
						<div class="form-label generic">
							<label for="first-name">First Name</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['first_name']; ?>
						</div>
					</div>

					<div class="form-row">
						<div class="form-label generic">
							<label for="last-name">Last Name</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['last_name']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="street-number">Street Number</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['streetNumber']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="street-name">Street Name</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['streetName']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="suburb">Suburb</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['suburb']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="state">State</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['state']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="country">Country</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['country']; ?>
						</div>
					</div>
					
					<div class="form-row">
						<div class="form-label generic">
							<label for="post-code">Post Code</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['postCode']; ?>
						</div>
					</div>
					
				</div>
				<div id="right-side">
					<div class="form-row generic-row">
						<div class="form-label generic">
							<label for="home-phone">Home Phone</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['homePhone']; ?>
						</div>
					</div>

					<div class="form-row">
						<div class="form-label generic">
							<label for="mobile-phone">Mobile Phone</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['mobilePhone']; ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-label generic">
							<label for="qualification">Qualifications</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['qualifications']; ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-label generic">
							<label for="sex">Sex</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['sex']; ?>
						</div>
					</div>

					<div class="form-row">
						<div class="form-label generic">
							<label for="date-of-birth">Date Of Birth</label>
						</div>
						<div class="form-input generic">
							<?php echo $staffDetails['DOB']; ?>
						</div>
					</div>
					<div class="form-row">
						<div class="form-label generic">
							<label for="position">Position (NYI) </label>
						</div>
						<div class="form-input generic">
							Staff
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="form-button">
						<!-- change this to a styled link -->
						<button onclick="window.location.href='edit.php?m=staff&amp;id=<?php echo $staffDetails["staffID"]; ?>'">Edit this user</button>
					</div>
				</div>
				<?php
					} else {
				?>
				<h1>Achtung, this user is protected</h1>
				<?php
					}
				?>
			</div>
<?php
	// Page content end
	include "inc/footer.php";
?>
