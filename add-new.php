<?php
	include "inc/header.php";
?>
<?php
	include "inc/menu.php";
	// Page content start
?>
		<div id="content-container">
			<div id="sub-page-menu">
				<ul>
					<li><a href="<?php echo ($_GET["m"]=="staff") ? "staff.php" : "patients.php"; ?>">&laquo; Go back</a></li>
				</ul>
			</div>
			<div id="entry-form">
				<h1>New <?php echo ($_GET["m"]=="staff") ? "Staff" : "Patient"; ?> Form</h1>
				<?php if($_GET['m']=="staff") { ?>
				<form action="handles/add-new-staff.php" method="POST">
				<?php } else { ?>
				<form action="handles/add-new-patient.php" method="POST">
				<?php } ?>
					<div id="left-side">
						<div class="form-row generic-row">
							<div class="form-label generic">
								<label for="first-name">First Name</label>
							</div>
							<div class="form-input generic">
								<input id="first-name" type="text" placeholder="First Name" name="first-name"/>
							</div>
						</div>

						<div class="form-row">
							<div class="form-label generic">
								<label for="last-name">Last Name</label>
							</div>
							<div class="form-input generic">
								<input id="last-name" type="text" placeholder="Last Name" name="last-name"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="street-number">Street Number</label>
							</div>
							<div class="form-input generic">
								<input id="street-number" type="text" placeholder="Last Name" name="street-number"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="street-name">Street Name</label>
							</div>
							<div class="form-input generic">
								<input id="street-name" type="text" placeholder="Street Name" name="street-name"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="suburb">Suburb</label>
							</div>
							<div class="form-input generic">
								<input id="suburb" type="text" placeholder="Suburb" name="suburb"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="state">State</label>
							</div>
							<div class="form-input generic">
								<input id="state" type="text" placeholder="State" name="state"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="country">Country</label>
							</div>
							<div class="form-input generic">
								<input id="country" type="text" placeholder="country" name="country"/>
							</div>
						</div>
						
						<div class="form-row">
							<div class="form-label generic">
								<label for="post-code">Post Code</label>
							</div>
							<div class="form-input generic">
								<input id="post-code" type="text" placeholder="Post Code" name="post-code"/>
							</div>
						</div>
						
					</div>
					<div id="right-side">
						<div class="form-row generic-row">
							<div class="form-label generic">
								<label for="home-phone">Home Phone</label>
							</div>
							<div class="form-input generic">
								<input id="home-phone" type="text" placeholder="Home Phone" name="home-phone"/>
							</div>
						</div>

						<div class="form-row">
							<div class="form-label generic">
								<label for="mobile-phone">Mobile Phone</label>
							</div>
							<div class="form-input generic">
								<input id="mobile-phone" type="text" placeholder="Mobile Phone" name="mobile-phone"/>
							</div>
						</div>

						<?php
							if($_GET['m']=="staff") {
						?>
						<div class="form-row">
							<div class="form-label generic">
								<label for="qualification">Qualifications</label>
							</div>
							<div class="form-input generic">
								<input id="qualification" type="text" placeholder="Qualifications" name="qualification"/>
							</div>
						</div>
						<?php
							}
						?>
						<div class="form-row">
							<div class="form-label generic">
								<label for="sex">Sex</label>
							</div>
							<div class="form-input generic">
								<select name="sex" id="sex">
									<option value="male">Male</option>
									<option value="female">Female</option>
								</select>
							</div>
						</div>

						<div class="form-row">
							<div class="form-label generic">
								<label for="date-of-birth">Date Of Birth</label>
							</div>
							<div class="form-input generic">
								<input id="date-of-birth" type="date" name="date-of-birth"/>
							</div>
						</div>
						<?php
							if($_GET['m']=="staff") {
						?>
						<div class="form-row">
							<div class="form-label generic">
								<label for="position">Position</label>
							</div>
							<div class="form-input generic">
								<select name="position" id="position">
									<option value="doctor">Doctor</option>
									<option value="nurse">Nurse</option>
									<option value="receptionist">Receptionist</option>
									<option value="whatever">Whatever</option>
								</select>
							</div>
						</div>
						<?php
							}
						?>

					</div>
					<div class="form-row">
						<div class="form-button">
							<button>Add New Staff Member</button>
						</div>
					</div>
				</form>
			</div>
		</div>
<?php
	// Page content end
	include "inc/footer.php";
?>
