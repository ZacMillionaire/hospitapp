<?php
	include "inc/header.php";
?>
<?php
	include "inc/menu.php";
	// Page content start
?>
		<div id="content-container">
			<div id="sub-page-menu">
				<ul>
					<li><a href="add-new.php?m=staff">Add New Staff Member</a></li>
				</ul>
			</div>
			<?php
				include "data/staff-data.php";
				foreach ($data as $key => $value) {
			?>
				<div class="staff-block-container">
					<div class="staff-block-letter-container"><?php echo $key; ?></div>
					<div class="staff-block-inner-container">
						<?php
							foreach ($value as $key => $value) {
						?>
						<div class="staff-entry-container">
							<div class="staff-name-container">
								<a href="view-staff.php?id=<?php echo $value["staffID"]; ?>">
									<span class="last-name"><?php echo $value['last_name']; ?>, </span><span class="first-name"><?php echo $value['first_name']; ?></span>
								</a>
							</div>
							<div class="staff-details-container">Some details about this staff member: (status, role, available etc)</div>
							<?php
							// if roles
							?>
							<div class="staff-edit"><a href="edit.php?m=staff&amp;id=<?php echo $value["staffID"]; ?>">Edit</a></div>
							<?php
							// end if roles
							?>
						</div>

						<?php
							} // end foreach data as values
						?>
					</div>
				</div>
			<?php
				} // end foreach data
			?>
		</div>
<?php
	// Page content end
	include "inc/footer.php";
?>
